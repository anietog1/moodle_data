# moodle_data

En este repositorio se almacenan los datos de moodle y algunos archivos de configuración para el deployment como son el .gitlab-ci, el archivo .env y varios docker-compose como el runner y el s3vol para el deployment y la configuración del runner en una instancia ec2.

Los scripts aquí pretenden proveer la siguiente arquitectura:

![arquitectura](/images/devops-V3.png)

## carpetas

### moodle: 

Archivos de moodle

### runner:

Archivos de configuración del runner en una instancia de s3. En el docker-compose se deben actualizar las credenciales y se debe ejecutar el ./register.sh

### s3vol:

Archivos de configuración de la conexión con s3